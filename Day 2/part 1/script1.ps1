[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')

$arrayLines = [System.Collections.ArrayList]@()

function VbsMsgBox 
{
 param( [string]$message, [int]$btnFlags, [string]$title )
 
 [Microsoft.VisualBasic.Interaction]::MsgBox($message, $btnFlags, $title)
}

function readInput
{
    foreach($line in Get-Content .\input1.txt) 
  {
      $arrayLines.Add($line)      
  }
}

function searchByOccurrences
{
param( [string]$string )

$includes2XChar = 2
$includes3XChar = 3
$result = 0
$is2X = $false
$is3X = $false

$tempArray = $string.ToCharArray()
foreach ($letter in $tempArray)
{ 
  $countLetter = ($tempArray | Where-Object {$_ -eq $letter} | Measure-Object).Count
  if ($countLetter -eq 3) { $is3X = $true }  
  if ($countLetter -eq 2) { $is2X = $true }     
 }
 
 if ($is3X) { $result = $result + $includes3XChar }  
 if ($is2X) { $result = $result + $includes2XChar }  
 
 return $result
 
}

function getChecksum
{
    $idsWithOccurrencesByTwo = 0
    $idsWithOccurrencesByThree = 0
    readInput
    foreach ($string in $arrayLines) 
    {
       $retVal = searchByOccurrences -string $string
       
       if ($retVal -eq 2) {$idsWithOccurrencesByTwo++}
       
       if ($retVal -eq 3) {$idsWithOccurrencesByThree++}
       
       if ($retVal -eq 5) 
       {
        $idsWithOccurrencesByTwo++
        $idsWithOccurrencesByThree++
        }
    }
    $checkSum = $idsWithOccurrencesByTwo * $idsWithOccurrencesByThree
    VbsMsgBox "The checksum is $checkSum" 0 "Checksum"
}

getChecksum





