[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')

$arrayLines = [System.Collections.ArrayList]@()

function VbsMsgBox 
{
 param( [string]$message, [int]$btnFlags, [string]$title )
 
 [Microsoft.VisualBasic.Interaction]::MsgBox($message, $btnFlags, $title)
}

function readInput
{
    foreach($line in Get-Content ".\input2.txt") 
  {
      $arrayLines.Add($line)      
  }
}

function stringsMatch                   # return -1 if the strings do not match the pattern - a single char diiferent at the same position in both string
{                                       # return the position index of the different char when the strings match the pattern
    param ([string]$s1, [string]$s2)     
    $a1 = $s1
    $a2 = $s2
    $nonMatchingPosNo = 0
    $position = -1    
    for ($i=0; $i -lt $a1.Length; $i++)
    {
        if ($a1[$i] -ne $a2[$i])
        {
            $nonMatchingPosNo++
            if ($nonMatchingPosNo -gt 1) {return -1}
            $position = $i          
        }
    }    
    if ($nonMatchingPosNo -eq 0) {return -1}
    if ($nonMatchingPosNo -eq 1) {return $position}          
}

function getCommonIDs
{
    readInput
    $irange = $arrayLines.Count - 2
    $jrange = $arrayLines.Count - 1    
    for ($i=0; $i -lt $irange; $i++)
        {
            for ($j=$i+1; $j -lt $jrange; $j++)
            {
                $a = $arrayLines[$i]
                $b = $arrayLines[$j]
                $retVal = stringsMatch -s1 $a -s2 $b
                if ($retVal -ne -1)
                {
                    [System.Collections.ArrayList]$idValue = $arrayLines[$i].ToCharArray()                    
                    $idValue.RemoveAt($retVal)                    
                    VbsMsgBox "ID value is $idValue"                    
                }
            }
        }
}

getCommonIDs





