﻿[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')

function VbsMsgBox 
{
 param( [string]$message, [int]$btnFlags, [string]$title )
 
 [Microsoft.VisualBasic.Interaction]::MsgBox($message, $btnFlags, $title)
}

function getDuplicatedResFreq
{
  $resFreq = 0  
  $i=-1
  $array = [System.Collections.ArrayList]@()
  $arrayLines = [System.Collections.ArrayList]@()

 #$array = @()
 #$arrayLines = @()

  foreach($line in Get-Content ".\input2.txt") 
  {
      $i++
      $arrayLines.Add($line)   
      $resFreq = $resFreq + $line

        foreach ($item in $array)
        {
             #  VbsMsgBox "Array curent1 este $item" 0 "aa"
         if ($resFreq -eq $item)
         {
            $ln = $i + 1
           VbsMsgBox "First duplicated frequency is $item obtained at line $ln with value $line" 0 "$item"
            return $item
         }        
        }
        $array.Add($resFreq)     
  }
  
  $loop = $true
  $currentIndex = $i 
  $linesNo = $i + 1

  while ($loop)
    {                      
      $currentIndexArrayLines = (($currentIndex+1) % $linesNo)
      $a = $array[$currentIndex]
      $b = $arrayLines[$currentIndexArrayLines]
      $currentItem = $a + $b
               
      foreach ($item in $array)
      {
        if ($currentItem -eq $item)
        {
          $loop = $false
          VbsMsgBox "First duplicated frequency is $currentItem current index is $currentIndex" 0 "$currentItem"
          return $currentItem
        }
      }

      $array.Add($currentItem)          
      $currentIndex++    
    }  
}     

getDuplicatedResFreq