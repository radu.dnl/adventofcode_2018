[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')

function VbsMsgBox 
{
 param( [string]$message, [int]$btnFlags, [string]$title )
 
 [Microsoft.VisualBasic.Interaction]::MsgBox($message, $btnFlags, $title)
}

$resFreq = 0
$i=0

foreach($line in Get-Content .\input1.txt) 
{
    $resFreq = $resFreq + [int]$line
    $i++
    if ([Int]$line -isnot [Int]) {VbsMsgBox "Line $i has not an integer value" 0 "error"}
}

VbsMsgBox "Resulting Frequency is $resFreq" 0 "Resulted Frequency"
      
    
    
  
